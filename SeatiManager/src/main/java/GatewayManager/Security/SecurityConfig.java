package GatewayManager.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.csrf.CsrfLogoutHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;


@Configuration
//@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    public void configureLogin(AuthenticationManagerBuilder authenticationMgr)throws Exception{
        authenticationMgr.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder());
//                .usersByUsernameQuery("select username, password, enabled from users where username=?")
//                .authoritiesByUsernameQuery("select username, role from authorities where username=?");

    }

    @Override
    protected void configure(HttpSecurity http)throws Exception{

        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/main*").hasRole("USER")
                .antMatchers("/main*").hasRole("ADMIN")
                .antMatchers("/stats*").hasRole("USER")
                .antMatchers("/stats*").hasRole("ADMIN")
                .antMatchers("/admin*").hasRole("ADMIN")
                .antMatchers("/seati").permitAll()
                .antMatchers("/error").permitAll()
                .and()
                .httpBasic()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/seati.html")
                .deleteCookies("JSESSIONID").clearAuthentication(true)
                .invalidateHttpSession(true)
                .and()
                .headers()
                    .defaultsDisabled()
                    .cacheControl();
    }
}
