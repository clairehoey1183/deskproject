package GatewayManager.business;


import GatewayManager.dao.AdminDao;
import GatewayManager.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
public class AdminBS {

    @Autowired
    AdminDao dao;

    public List<Employee> getAllEmployees() {

        return dao.getAllEmployees();
    }

    public String removeEmployee(String soeID) {

        return dao.removeEmployee(soeID);
    }

    // encodes admin password before being added to db
    public int addAdmin(AdminUser adminUser) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        adminUser.setPassw(passwordEncoder.encode(adminUser.getPassw()));
        return dao.addAdmin(adminUser);
    }

    public List<AdminUser> getAllAdminUsers(){
        return dao.getAllAdminUsers();
    }

    public String removeAdminUser(String userName){
        return dao.removeAdminUser(userName);
    }

    public List<UnassignedDesk> getAllUnassignedDesks(){
        return dao.getAllUnassignedDesks();
    }

    public AllDesksAndOccupied addDesk(AllDesksAndOccupied desk){
        return dao.addDesk(desk);
    }

    public int removeDesk(int deskID){
        return dao.removeDesk(deskID);
    }

    public List<Notification> getNotifications(){

        return dao.getAllNotifications();
    }

    public List<Neighbour> getNeighbourhoods(){

        return dao.getNeighbourhoods();
    }

    public String changeNeighbourhood(Neighbour neighbour){
        return dao.changeNeighbourhood(neighbour);
    }

    public int changeNotification(Notification note){
        return dao.updateNotification(note);
    }

    public int addNotification(Notification note){
        return dao.addNotification(note);
    }

    public int removeNotification(int id){
        return dao.removeNotification(id);
    }

}
