package GatewayManager.business;

import GatewayManager.dao.GatewayDao;
import GatewayManager.dto.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GatewayBS {

    private static final Logger logger = LogManager.getLogger(GatewayBS.class);

    @Autowired
    GatewayDao dao;

    public List<AllDesksAndOccupied> findOccupiedDesks(String id) {
        return dao.findOccupiedDesks(id);
    }

    // method adds desk occupant history to database at 11am mon-fri
    @Scheduled(cron="0 0 11 * * MON-FRI")
    public List<OccupiedDesk>returnOccupiedDetails(){

        List<OccupiedDesk>listOfOccupants = dao.returnOccupiedDetails();

        for(OccupiedDesk occupant : listOfOccupants){
            dao.addOccupantHistoryToDb(occupant);
        }

        logger.info("Occupant history added to database");

        return dao.returnOccupiedDetails();
    }

    public List<OccupantHistory> getDeskHistory(int deskID){
       return dao.getDeskHistory(deskID);
    }

    public List<OccupiedDesk> findPerson(String firstName, String lastName) {

            return dao.findPerson(firstName, lastName);
    }

    public List <DeskByNeighbourhood> getDesksByNeighbourhood(int id){
        return dao.getDesksByNeighbourhood(id);
    }

}
