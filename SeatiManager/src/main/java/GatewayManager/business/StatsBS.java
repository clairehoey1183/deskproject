package GatewayManager.business;

import GatewayManager.dao.StatsDao;
import GatewayManager.dto.CapacityMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

@Component
public class StatsBS {

    private static final Logger logger = LogManager.getLogger(StatsBS.class);

    @Autowired
    StatsDao dao;


    // method is scheduled to run on the hour between 8 and 6 on weekdays only
    // calculates current capacity of building and calls 'storeCapacityStats' method to push data to database
    @Scheduled(cron="0 0 8-18 * * MON-FRI")
    public double getCapacityCount(){

        double occupied =  dao.getCapacityCountOccupied();
        double desks = dao.getCapacityCountDesks();

        double capacity = (occupied/desks)*100;

        dao.storeCapacityStats(capacity);

        logger.info("Capacity stored to database");

        return capacity;
    }


    public List<String> getSections(){
        return dao.getSections();
    }


    // calls list of all sections and calculates capacity for each one, storing results in Treemap
    public Map<String, Double> getCapacityPerSection() {

        double capacity=0;
        double totalDeskCapacity=0;
        double occupiedDeskCapacity=0;
        Map<String, Double> capacityPerSection = new TreeMap<>();

        List<String> sections = dao.getSections();
        Map<String, Double> desks = dao.getCountForEachSection();
        Map<String, Double> occupied = dao.getCapacityPerSection();

        for (String section : sections) {
            totalDeskCapacity = desks.get(section);
            occupiedDeskCapacity = occupied.get(section);

            capacity= (occupiedDeskCapacity/totalDeskCapacity)*100;

            capacityPerSection.put(section,capacity);

        }

        return capacityPerSection;
    }

    // method calls Map of timeframes and capacities from Dao layer and calculates minimum
    public String suggestBetterTimeBasedOnCapacity() {
        Map<String, Integer> suggestBetterTime = dao.suggestBetterTimeBasedOnCapacity();

        Entry<String, Integer> min = null;
        for (Entry<String, Integer> entry : suggestBetterTime.entrySet()) {
            if (min == null || min.getValue() > entry.getValue()) {
                min = entry;
            }
        }
        logger.info("Timeframe based on capacity statistics suggested");
        String timeframe = min.getKey();
        return timeframe;
    }

    public List<CapacityMap>averageCapacityByDate(){
       return dao.averageCapacityByDate();
    }

    public double getCapacityCountOccupied(){
        return dao.getCapacityCountOccupied();
    }

    public double getCapacityCountDesks(){
        return dao.getCapacityCountDesks();
    }

    public double getCountOfEmployees(){
        return dao.getCountOfEmployees();
    }

    // uses Dao methods of desk count - occupied desk count to calculate number of available desks
    public double getAvailableDeskCount(){

        double number_of_desks = dao.getCapacityCountDesks();
        double occupied_desks = dao.getCapacityCountOccupied();
        double available_desks = number_of_desks-occupied_desks;
        return available_desks;
    }

}
