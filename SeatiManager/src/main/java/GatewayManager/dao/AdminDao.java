package GatewayManager.dao;

import GatewayManager.dto.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServlet;
import java.sql.*;
import java.util.List;


@Component
public class AdminDao extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(AdminDao.class);

    @Autowired
    private JdbcTemplate tpl;

    //retrieves all employees from database
    public List<Employee> getAllEmployees() {
        return tpl.query("SELECT * FROM employees ORDER BY lastName", new AdminDao.EmployeeMapper());
    }

    //retrieves all notifications from database
    public List<Notification> getAllNotifications(){
        return tpl.query("SELECT * FROM notification", new AdminDao.NotificationMapper());
    }

    //retrieves all desks that have not been assigned a grid position
    public List<UnassignedDesk> getAllUnassignedDesks(){
        return tpl.query("SELECT desks.deskid AS unassignedDeskId, desk_map.deskID AS deskId FROM desks LEFT JOIN desk_map ON desks.deskid = desk_map.deskID WHERE desk_map.deskID IS NULL", new UnassignedDeskMapper());
    }

    //removes employee from database
    public String removeEmployee(String soeID) {
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("DELETE from employees where SOEid = ?",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, soeID);

                        return ps;
                    }
                }
        );
        logger.info("Deleted Record with ID = " + soeID );
        return soeID;
    }

    //add admin user to database
    public int addAdmin(AdminUser adminUser) {

        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("INSERT INTO users(username, password, enabled) VALUES (?, ?, 1)",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, adminUser.getUserName());
                        ps.setString(2, adminUser.getPassw());

                        return ps;
                    }
                }
                );
        addAdminAsAuthority(adminUser);
        logger.info("Admin added");
        return 0;
    }

    // adds new admin user as an authority in db - part of spring security
    public int addAdminAsAuthority(AdminUser adminUser) {

        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("INSERT INTO authorities(username, authority) VALUES (?, 'ROLE_ADMIN')",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, adminUser.getUserName());

                        return ps;
                    }
                }
    );
        return 0;
    }

    //return list of admin users
    public List<AdminUser> getAllAdminUsers(){
        return tpl.query("SELECT users.username, users.password FROM users LEFT JOIN authorities ON users.username = authorities.username WHERE authority = 'ROLE_ADMIN';", new AdminUserMapper());
    }

    // deletes admin access in db
    public String removeAdminUser(String userName) {
        removeAdminUserAsAuthority(userName);
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("DELETE from users where username = ?",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, userName);

                        return ps;
                    }
                }
        );

        logger.info("Deleted Admin User with ID = " + userName );
        return userName;
    }

    // deletes admin user as an authority in db - part of spring security
    public String removeAdminUserAsAuthority(String userName) {
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("DELETE from authorities where username = ?",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, userName);

                        return ps;
                    }
                }
        );
        return userName;
    }



    //adds desk to desk mapping system
    public AllDesksAndOccupied addDesk(AllDesksAndOccupied desk) {

        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("INSERT INTO desk_map (deskID, desk_column, desk_row ,section, neighbourhoodId) values (?, ?, ?, ?, ?);");

                        ps.setInt(1,desk.getDesk_id());
                        ps.setInt(2, desk.getDesk_column());
                        ps.setInt(3, desk.getDesk_row());
                        ps.setString(4, desk.getSection());
                        ps.setInt(5,desk.getNeighbourhoodId());

                        return ps;
                    }
                }
                );
        logger.info("Desk added to system");
        return desk;
    }

//    public double storeCapacityStats(double capacity){
//
//        String SQL = "INSERT INTO capacity_stats (capacity, time) VALUES (?, NOW())";
//        tpl.update(SQL, capacity);
//        return capacity;
//    }

    //removes desk from desk mapping system
    public int removeDesk(int deskID) {
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("delete from desk_map where deskID = ?",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setInt(1, deskID);

                        return ps;
                    }
                }
        );
        logger.info("Deleted Record with ID = " + deskID );
        return deskID;

    }

    // updates name of neighbourhood
    public String changeNeighbourhood(Neighbour neighbour) {

        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("UPDATE neighbourhood SET neighbourhood= ? WHERE id= ?;",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, neighbour.getName());
                        ps.setString(2, neighbour.getId());

                        return ps;
                    }
                },
                keyHolder);

        return neighbour.getId();
    }

    //gets all neighbourhoods from database
    public List<Neighbour> getNeighbourhoods(){
        return tpl.query("SELECT DISTINCT neighbourhood.id, neighbourhood.neighbourhood, desk_map.section from neighbourhood LEFT JOIN desk_map ON neighbourhood.id = desk_map.neighbourhoodId ORDER BY neighbourhood.id;", new NeighbourMapper());
    }

    //updates notification
    public int updateNotification(Notification note) {

        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("UPDATE notification SET notification= ? WHERE id = ?;",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, note.getNotification());
                        ps.setInt(2, note.getId());
                        return ps;
                    }
                },
                keyHolder);

        return note.getId();
    }


    // add notification to database
    public int addNotification(Notification note) {

        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("Insert into notification(notification) values (?)",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, note.getNotification());

                        return ps;
                    }
                },
                keyHolder);
        return keyHolder.getKey().intValue();
    }

    // remove notification
    public int removeNotification(int id) {
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("delete from notification where id = ?",
                                        Statement.RETURN_GENERATED_KEYS);
                        ps.setInt(1, id);
                        return ps;
                    }
                }
                );
        logger.info("Deleted notification with ID = " + id );
        return id;

    }


    //mappers for each GET sql query
    private static final class NeighbourMapper
            implements RowMapper<Neighbour> {
        public Neighbour mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            Neighbour neighbour = new Neighbour
                    (rs.getString("id"),
                            rs.getString("neighbourhood"),
                            rs.getString("section"));


            return neighbour;
        }

    }

    private static final class EmployeeMapper
            implements RowMapper<Employee> {
        public Employee mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            Employee emp = new Employee
                    (rs.getString("SOEid"),
                            rs.getString("firstName"),
                            rs.getString("lastName"));

            return emp;
        }

    }

    private static final class NotificationMapper
            implements RowMapper<Notification> {
        public Notification mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            Notification note = new Notification
                    (rs.getInt("id"),
                            rs.getString("notification"));

            return note;
        }

    }

        private static final class UnassignedDeskMapper
            implements RowMapper<UnassignedDesk> {
        public UnassignedDesk mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            UnassignedDesk desk = new UnassignedDesk
                    (rs.getInt("deskId"),
                            rs.getInt("unassignedDeskId"));

            return desk;
        }

    }

    private static final class AdminUserMapper
            implements RowMapper<AdminUser> {
        public AdminUser mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            AdminUser admin = new AdminUser
                    (rs.getString("username"),
                    rs.getString("password"));
            return admin;
        }

    }

}
