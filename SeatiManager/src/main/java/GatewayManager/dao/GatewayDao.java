package GatewayManager.dao;

import GatewayManager.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServlet;
import java.sql.*;
import java.util.List;




@Component
public class GatewayDao extends HttpServlet {

    @Autowired
    private JdbcTemplate tpl;


    // returns left join of all desks with occupied desks - if occupied column is null system will know desk is available
    public List<AllDesksAndOccupied> findOccupiedDesks(String id) {
        return tpl.query("SELECT desk_map.deskID , desk_map.desk_column, desk_map.desk_row, desk_map.neighbourhoodId, desk_map.section, occupied.assetNo AS occupied FROM desk_map LEFT JOIN desks ON desks.deskid = desk_map.deskID LEFT JOIN occupied ON desks.assetNo = occupied.assetNo WHERE desk_map.section ='"+id+"' ORDER BY desk_map.deskID;", new AllDesksAndOccupiedDeskMapper());
    }

    // returns desks that are contained in a specified neighbourhood
    public List <DeskByNeighbourhood> getDesksByNeighbourhood(int id){
        return tpl.query("SELECT deskID, neighbourhoodId, section FROM desk_map WHERE neighbourhoodId ="+id+";", new DeskByNeighbourhoodMapper());
    }


    // returns details of occupied desk, asset, name of person occupying and date
    public List<OccupiedDesk> returnOccupiedDetails() {

        return tpl.query("SELECT desks.deskID, occupied.assetNo, ip.IPAddress, employees.firstName, employees.lastName, employees.SOEid, occupied.date FROM desks \n" +
                "INNER JOIN occupied ON desks.assetNo = occupied.assetNo\n" +
                "LEFT JOIN employees ON employees.id = occupied.personID\n" +
                "LEFT JOIN ip ON ip.IPAddressID = desks.ipAddressID;", new OccupiedMapper());
    }

    // takes collated details of occupied desk and stores them back in to seperate table in database
    public int addOccupantHistoryToDb(OccupiedDesk occupant){
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement("INSERT into occupant_history(deskID, FirstName, LastName, soeId, Date) values(?,?,?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);
                        ps.setInt(1, occupant.getDeskID());
                        ps.setString(2, occupant.getFirstName());
                        ps.setString(3, occupant.getLastName());
                        ps.setString(3, occupant.getSoeId());
                        ps.setDate(4, occupant.getDate());

                        return ps;
                    }
                },
                keyHolder);
        return keyHolder.getKey().intValue();
    }

    // returns historical occupancy of a specified desk
    public List<OccupantHistory> getDeskHistory(int deskID){
        return tpl.query("SELECT * FROM occupant_history WHERE deskID ="+deskID+";", new OccupantHistoryMapper());
    }

    // returns id and position of specific person that has been searched for
    public List<OccupiedDesk> findPerson(String firstName, String lastName) {

        return tpl.query("SELECT * FROM desks \n" +
                "LEFT JOIN occupied ON desks.assetNo = occupied.assetNo\n" +
                "LEFT JOIN employees ON employees.SOEid = occupied.soeID\n" + "LEFT JOIN desk_map ON desk_map.deskID = desks.deskid "+
                "LEFT JOIN ip ON ip.IPAddressID = desks.ipAddressID WHERE firstName='"+firstName+"' AND lastName='"+lastName+"';", new OccupiedMapper());

    }



    private static final class OccupiedMapper
            implements RowMapper<OccupiedDesk> {
        public OccupiedDesk mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            OccupiedDesk occupiedDesk = new OccupiedDesk
                    (rs.getInt("deskid"),
                            rs.getString("firstName"),
                            rs.getString("lastName"),
                            rs.getString("SOEid"),
                            rs.getString("section"),
                            rs.getDate("date"));
            return occupiedDesk;
        }

    }

    private static final class OccupantHistoryMapper
            implements RowMapper<OccupantHistory> {
        public OccupantHistory mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            OccupantHistory occupantHistory = new OccupantHistory
                    (rs.getInt("deskid"),
                            rs.getString("firstName"),
                            rs.getString("lastName"),
                            rs.getString("SOEid"),
                            rs.getDate("date"));
            return occupantHistory;
        }

    }

    private static final class AllDesksAndOccupiedDeskMapper
            implements RowMapper<AllDesksAndOccupied> {
        public AllDesksAndOccupied mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            AllDesksAndOccupied desks = new AllDesksAndOccupied
                    (rs.getInt("deskID"),
                            rs.getInt("desk_column"),
                            rs.getInt("desk_row"),
                            rs.getInt("occupied"),
                            rs.getString("section"),
                            rs.getInt("neighbourhoodId"));

            return desks;
        }

    }

    private static final class DeskByNeighbourhoodMapper
            implements RowMapper<DeskByNeighbourhood> {
        public DeskByNeighbourhood mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            DeskByNeighbourhood deskByNeighbourhood = new DeskByNeighbourhood
                    (rs.getInt("deskID"),
                            rs.getInt("neighbourhoodId"),
                            rs.getString("section"));

            return deskByNeighbourhood;
        }

    }

}




