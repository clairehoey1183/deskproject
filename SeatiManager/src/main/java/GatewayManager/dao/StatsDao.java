package GatewayManager.dao;

import GatewayManager.dto.CapacityMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServlet;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class StatsDao extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(StatsDao.class);

    static final int CAPACITY_DELETION_DAYS=5;


    @Autowired
    private JdbcTemplate tpl;

    //counts the number of desks assigned to the desk map
    public double getCapacityCountDesks(){
        double count;
        count = tpl.queryForObject("SELECT COUNT(deskID) FROM desk_map", Double.class);
        return count;

    }

    // counts the number of occupied desks within the desk map
    public double getCapacityCountOccupied(){
        double count= tpl.queryForObject("SELECT COUNT(id) FROM occupied", Double.class);
        return count;
    }

    // counts number of employees currently registered
    public double getCountOfEmployees(){
        double count;
        count= tpl.queryForObject("SELECT COUNT(SOEid) FROM employees", Double.class);
        return count;
    }


    // calculates number of desks within each section and adds to map
    public Map<String, Double> getCountForEachSection(){

        List<String>sections = getSections();

        Map<String, Double> countOfDesksPerSection = new HashMap<>();

        for (String section: sections){
            countOfDesksPerSection.put(section,tpl.queryForObject("SELECT COUNT(deskID) FROM desk_map WHERE section='" + section+"'", Double.class));
        }

        return countOfDesksPerSection;

    }

    // calculates number of desks that are occupied within each section and adds to map
    public Map<String, Double> getCapacityPerSection(){

        List<String>sections = getSections();

        Map<String, Double> capacityOfDesksPerSection = new HashMap<>();

        for (String section: sections){
            capacityOfDesksPerSection.put(section,tpl.queryForObject("SELECT COUNT(desks.deskid) FROM desks INNER JOIN desk_map ON desks.deskid = desk_map.deskID JOIN occupied ON desks.assetNo = occupied.assetNo  WHERE section = '" + section+"'", Double.class));
        }

        return capacityOfDesksPerSection;

    }

    // returns list of distinct sections from db
    public List<String> getSections(){
        return tpl.queryForList("SELECT DISTINCT section FROM desk_map;", String.class);
    }

    // passed capacity value from StatsBs and adds to database with current time and date
    public double storeCapacityStats(double capacity){

        String SQL = "INSERT INTO capacity_stats (capacity, time) VALUES (?, NOW())";
        tpl.update(SQL, capacity);
        return capacity;
    }

    // scheduled to run every day at midnight
    // method checks database for any capacity statistics that are over 5 days old

    @Scheduled(cron="0 0 0 * * ?")
    public double deleteCapacityStatsOnFixedBasis(){

        String SQL = "DELETE from capacity_stats WHERE time < NOW() - INTERVAL "+CAPACITY_DELETION_DAYS+" DAY";

        tpl.update(SQL);
        logger.info("Capacities older than "+CAPACITY_DELETION_DAYS+" deleted from database");
        return 0;
    }

    // method queries database for average capacities between different time frames
    // adds each value to a HashMap

    public Map<String, Integer> suggestBetterTimeBasedOnCapacity(){

        int morning = tpl.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '7' and '10';", Integer.class);
        int mid_morning = tpl.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '10' and '12';", Integer.class);
        int lunch = tpl.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '12' and '14';", Integer.class);
        int afternoon = tpl.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '14' and '17';", Integer.class);

        Map <String, Integer> timings = new HashMap<>();

        timings.put("07.00 - 10.00", morning);
        timings.put("10.00 - 12.00", mid_morning);
        timings.put("12.00 - 14.00", lunch);
        timings.put("14.00 - 17.00",afternoon);

        return timings;
    }

    // method provides list of date and capacities from database
    public List<CapacityMap> averageCapacityByDate(){

        List<CapacityMap> capacityByDate = tpl.query("SELECT Date(time) AS capacityDate, avg(capacity) AS capacity FROM capacity_stats GROUP BY Date(time);", new CapacityMapper());

        return capacityByDate;
    }



    public static final class CapacityMapper
            implements RowMapper<CapacityMap> {
        public CapacityMap mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            CapacityMap capacityByDate = new CapacityMap
                            (rs.getString("capacityDate"),
                            rs.getInt("capacity"));

            return capacityByDate;
        }

    }


}
