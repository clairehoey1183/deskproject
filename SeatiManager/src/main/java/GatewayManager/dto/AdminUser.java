package GatewayManager.dto;

public class AdminUser {

    private String userName;
    private String passw;

    public AdminUser(){

    }

    public AdminUser(String userName, String passw) {
        this.userName = userName;
        this.passw = passw;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    @Override
    public String toString() {
        return "AdminUser{" +
                "userName='" + userName + '\'' +
                ", passw='" + passw + '\'' +
                '}';
    }
}
