package GatewayManager.dto;

public class AllDesksAndOccupied {

    private int desk_id;
    private int desk_column;
    private int desk_row;
    private int occupied;
    private int neighbourhoodId;
    private String section;

    public AllDesksAndOccupied(){

    }


    public AllDesksAndOccupied(int desk_id, int desk_column, int desk_row, int occupied, String section, int neighbourhoodId) {
        this.desk_id = desk_id;
        this.desk_column = desk_column;
        this.desk_row = desk_row;
        this.occupied = occupied;
        this.section = section;
        this.neighbourhoodId = neighbourhoodId;
    }

    public int getDesk_column() {
        return desk_column;
    }

    public void setDesk_column(int desk_column) {
        this.desk_column = desk_column;
    }

    public int getDesk_row() {
        return desk_row;
    }

    public void setDesk_row(int desk_row) {
        this.desk_row = desk_row;
    }

    public int getDesk_id() {
        return desk_id;
    }

    public void setDesk_id(int desk_id) {
        this.desk_id = desk_id;
    }

    public int getOccupied() {
        return occupied;
    }

    public void setOccupied(int occupied) {
        this.occupied = occupied;
    }

    public int getNeighbourhoodId() {
        return neighbourhoodId;
    }

    public void setNeighbourhoodId(int neighbourhoodId) {
        this.neighbourhoodId = neighbourhoodId;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public String toString() {
        return "AllDesksAndOccupied{" +
                "desk_id=" + desk_id +
                ", desk_column=" + desk_column +
                ", desk_row=" + desk_row +
                ", occupied=" + occupied +
                ", neighbourhoodId=" + neighbourhoodId +
                ", section='" + section + '\'' +
                '}';
    }
}
