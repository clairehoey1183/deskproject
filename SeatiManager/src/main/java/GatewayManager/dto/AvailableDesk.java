package GatewayManager.dto;

public class AvailableDesk {

    private int deskID;

    public AvailableDesk() {

    }

    public AvailableDesk(int deskID) {
        this.deskID = deskID;
    }

    public int getDeskID() {
        return deskID;
    }

    public void setDeskID(int deskID) {
        this.deskID = deskID;
    }


}


