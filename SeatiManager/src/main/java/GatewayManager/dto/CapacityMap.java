package GatewayManager.dto;

public class CapacityMap {

    private String capacityDate;
    private int capacity;

    public CapacityMap(String date, int capacity) {
        this.capacityDate = date;
        this.capacity = capacity;
    }


    public String getCapacityDate() {
        return capacityDate;
    }

    public void setCapacityDate(String capacityDate) {
        this.capacityDate = capacityDate;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "CapacityMap{" +
                "capacityDate='" + capacityDate + '\'' +
                ", capacity=" + capacity +
                '}';
    }
}
