package GatewayManager.dto;

public class DeskByNeighbourhood {

    private int deskID;
    private int neighbourhoodId;
    private String section;

    public DeskByNeighbourhood(int deskID, int neighbourhoodId, String section) {
        this.deskID = deskID;
        this.neighbourhoodId = neighbourhoodId;
        this.section = section;
    }

    public int getDeskID() {
        return deskID;
    }

    public void setDeskID(int deskID) {
        this.deskID = deskID;
    }

    public int getNeighbourhoodId() {
        return neighbourhoodId;
    }

    public void setNeighbourhoodId(int neighbourhoodId) {
        this.neighbourhoodId = neighbourhoodId;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public String toString() {
        return "DeskByNeighbourhood{" +
                "deskID=" + deskID +
                ", neighbourhoodId=" + neighbourhoodId +
                ", section='" + section + '\'' +
                '}';
    }
}
