package GatewayManager.dto;

public class Employee {

    private String soeID;
    private String firstName;
    private String lastName;


    public Employee() {

    }

    public Employee(String soeID, String firstName, String lastName) {
        this.soeID = soeID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getSoeID() {
        return soeID;
    }

    public void setSoeID(String soeID) {
        this.soeID = soeID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "soeID=" + soeID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
