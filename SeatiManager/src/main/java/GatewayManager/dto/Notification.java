package GatewayManager.dto;

public class Notification {

    private int id;
    private String notification;

    public Notification() {

    }

    public Notification(int id, String notification) {
        this.id = id;
        this.notification = notification;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
