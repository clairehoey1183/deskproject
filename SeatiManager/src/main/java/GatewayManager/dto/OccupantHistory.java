package GatewayManager.dto;

import java.sql.Date;

public class OccupantHistory {

    private int deskID;
    private String firstName;
    private String lastName;
    private String soeId;
    private Date date;

    public OccupantHistory(int deskID, String firstName, String lastName, String soeId, Date date) {
        this.deskID = deskID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.soeId = soeId;
        this.date = date;
    }

    public int getDeskID() {
        return deskID;
    }

    public void setDeskID(int deskID) {
        this.deskID = deskID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSoeId() {
        return soeId;
    }

    public void setSoeId(String soeId) {
        this.soeId = soeId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "OccupantHistory{" +
                "deskID=" + deskID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", soeId='" + soeId + '\'' +
                ", date=" + date +
                '}';
    }
}
