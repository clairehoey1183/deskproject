package GatewayManager.dto;

import java.sql.Date;

public class OccupiedDesk {

    private int deskID;
    private String firstName;
    private String lastName;
    private String soeId;
    private String section;
    private Date date;


    public OccupiedDesk(int deskID,  String firstName, String lastName, String soeId, String section,Date date) {
        this.deskID = deskID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.soeId = soeId;
        this.section = section;
        this.date = date;
    }

    public int getDeskID() {
        return deskID;
    }

    public void setDeskID(int deskID) {
        this.deskID = deskID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSoeId() {
        return soeId;
    }

    public void setSoeId(String soeId) {
        this.soeId = soeId;
    }

    @Override
    public String toString() {
        return "OccupiedDesk{" +
                "deskID=" + deskID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", soeId='" + soeId + '\'' +
                ", section='" + section + '\'' +
                ", date=" + date +
                '}';
    }
}

