package GatewayManager.dto;

public class UnassignedDesk {

    private int deskId;
    private int unassignedDeskId;

    public UnassignedDesk(){

    }

    public UnassignedDesk(int deskId, int unassignedDeskId) {
        this.deskId = deskId;
        this.unassignedDeskId = unassignedDeskId;
    }

    public int getDeskId() {
        return deskId;
    }

    public void setDeskId(int deskId) {
        this.deskId = deskId;
    }

    public int getUnassignedDeskId() {
        return unassignedDeskId;
    }

    public void setUnassignedDeskId(int unassignedDeskId) {
        this.unassignedDeskId = unassignedDeskId;
    }

    @Override
    public String toString() {
        return "UnassignedDesk{" +
                "deskId=" + deskId +
                ", unassignedDeskId=" + unassignedDeskId +
                '}';
    }
}