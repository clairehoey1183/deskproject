package GatewayManager.rest;

import GatewayManager.business.AdminBS;
import GatewayManager.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AdminBS bs;

    @RequestMapping(value = "/getAllEmployees")
    public List<Employee> getAllEmployees() {

        return bs.getAllEmployees();

    }

    @RequestMapping(value="/seatiRemovePerson")
    public String removeEmployee(String soeID){

        return bs.removeEmployee(soeID);
    }

    @RequestMapping(value="/getUnassignedDesks")
    public List<UnassignedDesk> getAllUnassignedDesks(){
        return bs.getAllUnassignedDesks();
    }


    @RequestMapping(value = "/addAdmin", method = RequestMethod.POST)
    public int addAdmin(@RequestBody AdminUser adminUser) {

        return bs.addAdmin(adminUser);
    }

    @RequestMapping(value="/getAllAdminUsers")
    public List<AdminUser> getAllAdminUsers(){
        return bs.getAllAdminUsers();
    }

    @RequestMapping(value="/removeAdminUser")
    public String removeAdminUser(String userName){

        return bs.removeAdminUser(userName);
    }

    @RequestMapping(value = "/addDesk", method = RequestMethod.POST)
    public AllDesksAndOccupied addDesk(@RequestBody AllDesksAndOccupied desk) {

        return bs.addDesk(desk);

    }

    @RequestMapping(value = "/removeDesk")
    public int removeDesk(int deskID) {

        return bs.removeDesk(deskID);

    }

    @RequestMapping(value = "/getNotifications")
    public List<Notification> getNotifications() {

        return bs.getNotifications();

    }

    @RequestMapping(value = "/getNeighbourhoods")
    public List<Neighbour> getNeighbourhoods() {

        return bs.getNeighbourhoods();

    }

    @RequestMapping(value = "/changeNeighbourhood", method = RequestMethod.PUT)
    public String changeNeighbourhood(@RequestBody Neighbour neighbour) {
        System.out.println("Neighbourhood is: " + neighbour.getId() + " : " + neighbour.getName());
        return bs.changeNeighbourhood(neighbour);

    }

    @RequestMapping(value = "/changeNotification", method = RequestMethod.PUT)
    public int changeNotification(@RequestBody Notification note) {
        return bs.changeNotification(note);

    }

    @RequestMapping(value = "/addNotification", method = RequestMethod.POST)
    public int addNotification(@RequestBody Notification note) {

        return bs.addNotification(note);

    }

    @RequestMapping(value = "/removeNotification")
    public int removeNotification(int id) {

        return bs.removeNotification(id);

    }


}
