package GatewayManager.rest;

import GatewayManager.business.GatewayBS;
import GatewayManager.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/desk")
public class GatewayController {

    @Autowired
    GatewayBS bs;

    @RequestMapping(value = "/returnAllOccupiedAndAvailableDesks", method = RequestMethod.GET)
    public List<AllDesksAndOccupied> returnAllOccupiedAndAvailableDesks(String id) {
        return bs.findOccupiedDesks(id);
    }


    @RequestMapping(value = "/getDeskHistory", method = RequestMethod.GET)
    public List<OccupantHistory> getDeskHistory(@RequestParam(required = false)int deskID){
        return bs.getDeskHistory(deskID);
    }


    @RequestMapping(value = "/seatiFindPerson", method = RequestMethod.GET)
    public List <OccupiedDesk> findPerson(@RequestParam("firstName")String firstName, @RequestParam("lastName")String lastName) {

        return bs.findPerson(firstName, lastName);

    }


    @RequestMapping(value = "/getDesksByNeighbourhood", method = RequestMethod.GET)
    public List <DeskByNeighbourhood> getDesksByNeighbourhood(int id){
        return bs.getDesksByNeighbourhood(id);
    }


}






