package GatewayManager.rest;

import GatewayManager.business.StatsBS;
import GatewayManager.dto.CapacityMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/stats")
public class StatsController {

    @Autowired
    StatsBS bs;


    @RequestMapping(value = "/getSections", method = RequestMethod.GET)
    public List<String> getSections() {

        return bs.getSections();
    }

    @RequestMapping(value = "/getCapacity")
    public double getCapacityCount() {

        return bs.getCapacityCount();

    }

    @RequestMapping(value = "/getCapacityPerSection")
    public Map<String, Double> getCapacityPerSection() {

        Map<String, Double> map = bs.getCapacityPerSection();
        return bs.getCapacityPerSection();

    }


    @RequestMapping(value = "/suggestBetterTime")
    public String suggestBetterTimeBasedOnCapacity() {

        return bs.suggestBetterTimeBasedOnCapacity();

    }

    @RequestMapping(value = "/capacityByDate")
    public List<CapacityMap>averageCapacityByDate(){
        return bs.averageCapacityByDate();
    }


    @RequestMapping(value = "/capacityCountOccupied")
    public double getCapacityCountOccupied(){
        return bs.getCapacityCountOccupied();
    }

    @RequestMapping(value = "/capacityCountDesks")
    public double getCapacityCountDesks(){
        return bs.getCapacityCountDesks();
    }

    @RequestMapping(value = "/countOfEmployees")
    public double getCountOfEmployees(){
        return bs.getCountOfEmployees();
    }

    @RequestMapping(value = "/countOfAvailableDesks")
    public double getAvailableDeskCount(){
        return bs.getAvailableDeskCount();
    }


}
