$(document).ready(startup);

function startup(){

	getNotifications();
	getEmployees();
	getNeighbourhoods();
	getUnassignedDesks();
	getAllAdminUsers();

	}


// displays Employee drop down
function getEmployees() {

	$.ajax({
		url : "/admin/getAllEmployees",
		//data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		    var output ="<select required id='getAllEmployees'>";

            		        output +="<option disabled selected value> --Employees--</option>";

            			        for (var i=0; i<response.length; i++){

                                   output += "<option value='"+response[i].soeID+"'>"+response[i].lastName+", "+response[i].firstName+" ("+response[i].soeID+")</option>";

            			        }
                        output += "</select>";

   			$("#getEmployees").html(output);
        		},
        		error : function(err) {
        			alert("Error: " + err.responseText);
        		}
        	});

        }




// removes employee from database
function removeEmployee() {

    var params = {
        soeID : document.getElementById("getAllEmployees").value,
    }

	$.ajax({
		url : "/admin/seatiRemovePerson",
		data : params,
//		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

              var output = "Employee successfully removed from system";

              $("#removeEmployeeSuccess").html(output);

              setTimeout(function(){location.reload()}, 3000);

		    },
             error : function(err){

                    console.log("Error: "+err.responseText)

                    var output = "Unable to remove employee, please check inputs.  If the problem persists please contact administrator.";

                    $("#removeEmployeeFailure").html(output);
                    }

	});

}

//adds admin to db
function addAdmin() {

    var params = {
        userName : $("#username").val(),
        passw : $("#passw").val(),
    }

    var paramsJson = JSON.stringify(params);

	$.ajax({
		url : "/admin/addAdmin",
		data : paramsJson,
		dataType : "json",
		type : "POST",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
              var output = "New admin user successfully added to system";

                $("#addAdminSuccess").html(output);

                 setTimeout(function(){location.reload()}, 3000);

		},
        error : function(err){
            console.log("Error: "+err.responseText)

             var output = "Unable to add admin, please check inputs.  If the problem persists please contact system administrator.";

             $("#addAdminFailure").html(output);
            }
	});

}

// returns all admins from db
function getAllAdminUsers() {

	$.ajax({
		url : "/admin/getAllAdminUsers",
		//data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		    var output ="<select required id='getAllAdminUsers'>";

            		        output +="<option disabled selected value> --Admin Users--</option>";

            			        for (var i=0; i<response.length; i++){

                                   output += "<option value='"+response[i].userName+"'>"+response[i].userName+"</option>";

            			        }
                        output += "</select>";

   			$("#getAdminUsers").html(output);
        		},
        		error : function(err) {
        			alert("Error: " + err.responseText);
        		}
        	});

 }

//removes admin from db
 function removeAdminUser() {

     var params = {
         userName : document.getElementById("getAllAdminUsers").value,
     }

 	$.ajax({
 		url : "/admin/removeAdminUser",
 		data : params,
 //		dataType : "json",
 		type : "GET",
 		cache : false,
 		async : false,
 		contentType : 'application/json; charset=utf-8',
 		success : function(response) {

               var output = "Admin successfully removed from system";

               $("#removeAdminSuccess").html(output);

               setTimeout(function(){location.reload()}, 3000);

 		    },
              error : function(err){

                     console.log("Error: "+err.responseText)

                     var output = "Unable to remove admin, please check inputs.  If the problem persists please contact system administrator.";

                     $("#removeAdminFailure").html(output);
                     }

 	});

 }

// adds desk to desk mapping system
function addDesk() {

    var params = {

        desk_id : document.getElementById("getAllUnassignedDesks").value,
        desk_column : $("#column").val(),
        desk_row : $("#row").val(),
        neighbourhoodId : $("#neighbourhood").val(),
        section : $("#section").val()
    }

    var paramsJson = JSON.stringify(params);

	$.ajax({
		url : "/admin/addDesk",
		data : paramsJson,
		dataType : "json",
		type : "POST",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		if(response.desk_row > 24 || response.desk_column > 6){
		    var output = ("Desk added is beyond suggested Health & Safety capacity")
		}

		 else{output = "Desk successfully added to system"};

              $("#addDeskSuccess").html(output);

              setTimeout(function(){location.reload()}, 3000);

		},
        error : function(err){
            console.log("Error: "+err.responseText)

            var output = "Unable to add desk, please check inputs.  If the problem persists please contact administrator.";

            $("#addDeskFailure").html(output);
            }
	});

}

// displays a drop down of desks that have not been assigned mappings
function getUnassignedDesks(){

    $.ajax({
    		url : "/admin/getUnassignedDesks",
    		dataType : "json",
    		type : "POST",
    		cache : false,
    		async : false,
    		contentType : 'application/json; charset=utf-8',
    		success : function(response) {

    		 		    var output ="<select required id='getAllUnassignedDesks'>";

                         		        output +="<option disabled selected value> --Unassigned Desks--</option>";

                         			        for (var i=0; i<response.length; i++){

                                                output += "<option value='"+response[i].unassignedDeskId+"'>"+response[i].unassignedDeskId+"</option>";

                         			        }
                                     output += "</select>";

                			$("#getUnassignedDesks").html(output);

    		},
            error : function(err){
                console.log("Error: "+err.responseText)

                }
    	});


}


// removes desk from mapping system
function removeDesk() {

    var params = {
        deskID : $("#removeDeskID").val(),
    }

	$.ajax({
		url : "/admin/removeDesk",
		data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		        var output = "Desk removed from system";

                $("#removeDeskSuccess").html(output);

                setTimeout(function(){location.reload()}, 3000);

		},
		 error : function(err){
              console.log("Error: "+err.responseText)

              var output = "Unable to remove desk, please check inputs.  If the problem persists please contact administrator.";

              $("#removeDeskFailure").html(output);
                     }

	});

}

// displays all neighbourhoods in drop down list
function getNeighbourhoods() {

	$.ajax({
    		url : "/admin/getNeighbourhoods",
    		dataType : "json",
    		type : "GET",
    		cache : false,
    		async : false,
    		contentType : 'application/json; charset=utf-8',
    		success : function(response) {

		    var output ="<select required id='getAllNeighbourhoods'>";

            		        output +="<option disabled selected value> --Neighbourhoods--</option>";

            			        for (var i=0; i<response.length; i++){

                                   output += "<option value='"+response[i].id+"'>"+response[i].id+". "+response[i].name+"</option>";

            			        }
                        output += "</select>";

   			$("#getNeighbourhoods").html(output);
        		},
        		error : function(err) {
        			console.log("Error: " + err.responseText);
        		}
        	});

        }


// updates neighbourhood name
function changeNeighbourhood() {

     var params = {
            id: document.getElementById("getAllNeighbourhoods").value,
            name: $("#newNeighbour").val(),
         }

     var paramsJson = JSON.stringify(params);

 	$.ajax({
 		url : "/admin/changeNeighbourhood",
 		data: paramsJson,
 		dataType : "json",
 		type : "PUT",
 		cache : false,
 		async : false,
 		contentType : 'application/json; charset=utf-8',
 		success : function(response) {

 		var output = "Neighbourhood updated successfully";

        $("#changeNeighbourhoodSuccess").html(output);

        setTimeout(function(){location.reload()}, 3000);


 		},
         error : function(err){
            console.log("Error: "+err.responseText)

             var output = "Unable to update neighbourhood.  If the problem persists please contact administrator.";

             $("#changeNeighbourhoodFailure").html(output);
             }
 	});

 }


//updates selected notification
function changeNotification() {

    var params = {

        id : $("#notificationSelect").val(),
        notification : $("#newNotification").val()
    }

    var paramsJson = JSON.stringify(params);

	$.ajax({
		url : "/admin/changeNotification",
		data : paramsJson,
		dataType : "json",
		type : "PUT",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

                var output = "Notification updated successfully";

        		$("#updateNotificationSuccess").html(output);

        		setTimeout(function(){location.reload()}, 3000);

		},
        error : function(err){
            console.log("Error: "+err.responseText)
            var output = "Unable to update notification.  If the problem persists please contact administrator.";

             $("#updateNotificationFailure").html(output);
            }
	});

}

//adds new notification
function addNotification() {

    var params = {

        notification : $("#note").val(),

    }

    var paramsJson = JSON.stringify(params);

	$.ajax({
		url : "/admin/addNotification",
		data : paramsJson,
		dataType : "json",
		type : "POST",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

			var output = "Notification added successfully";

        		$("#addNotificationSuccess").html(output);

        		setTimeout(function(){location.reload()}, 3000);

		},
        error : function(err){
            console.log("Error: "+err.responseText)

            var output = "Something went wrong, please check your input.  If the problem persists contact administrator";

              $("#addNotificationFailure").html(output);
            }
	});

}

//displays all notifications
function getNotifications() {

	$.ajax({
		url : "/admin/getNotifications",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		    var output = "<Table><tr>";

            for (var i=0; i<response.length; i++){

                output+="<td>"+response[i].id+". "+response[i].notification+"</td></tr>";

                     }
                 output+="</Table>";

            			$("#getNotifications").html(output);
		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

// removes notifications
function deleteNotification() {

    var params = {
        id : $("#notificationSelect").val(),
    }

	$.ajax({
		url : "/admin/removeNotification",
		data : params,
		//dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		},
		error : function(err) {
        			console.log("Error: " + err.responseText);

        			var output = "Something went wrong, please check input and contact admin if problem persists.";

        			$("#getNoteDeletionFailure").html(output);
        		}

	});

}






