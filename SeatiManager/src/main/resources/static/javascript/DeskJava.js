
$(document).ready(startup);

function startup(){

	getNeighbourhoodDropdown();
	getBetterTime();
	getMainNotifications();
	getCapacityPerSectionMain();

	}

// retrieves all desk mappings and arranges grid layout
// determines desk colour, direction and neighbourhood image
function getDesks(id) {

    var params = {
        id : id,
    }

	$.ajax({
		url : "/desk/returnAllOccupiedAndAvailableDesks",
		data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		showBuildingImage(id);

		var e = document.getElementById ("neighbourSelect");
		var neighbourhoodId = e.options [e.selectedIndex] .id;

        var output ="<Table><tr>";

            var previous_column=1;

            var aisle_inserted = false;

			for (var i=0; i<response.length; i++){

             if(response[i].desk_column != previous_column)
                {
                    output += "</tr><tr>";
                    previous_column++;
                }

//                    output+= "<td><div class='desk_number'>"+response[i].deskid+"</div>";
                    output+= "<td>";
			        if(response[i].occupied==0){

                        if(response[i].desk_row%2==0){
                                if(neighbourhoodId==response[i].neighbourhoodId){
                                         output+= "<img id='"+response[i].desk_id+"' class='desk' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/availableNeighbourDesk.png' height='70px' width='70px'>";
                                 }
                                    else{ output+= "<img id='"+response[i].desk_id+"' class='desk' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/availableDesk.png' height='70px' width='70px'>";
                                         }
                                 }
                            else {
                                if(neighbourhoodId==response[i].neighbourhoodId){
                                          output+= "<img id='"+response[i].desk_id+"' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/availableNeighbourDesk.png' height='70px' width='70px'>";
                                           }
                                else{output+= "<img id='"+response[i].desk_id+"' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/availableDesk.png' height='70px' width='70px'>";
                                }
                            }
                        }
                     else{
                        if(response[i].desk_row%2==0){
                            if(neighbourhoodId==response[i].neighbourhoodId){
                                          output+= "<img id='"+response[i].desk_id+"' class='desk' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/unavailableNeighbourDesk.png' height='70px' width='70px'>";
                             }
                           else{ output+= "<img id='"+response[i].desk_id+"' class='desk' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/unavailableDesk.png' height='70px' width='70px'>";
                                }
                        }
                        else {
                            if(neighbourhoodId==response[i].neighbourhoodId){
                                          output+= "<img id='"+response[i].desk_id+"' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/unavailableNeighbourDesk.png' height='70px' width='70px'>";
                               }
                            else{output+= "<img id='"+response[i].desk_id+"' data-toggle='tooltip' data-placement='auto' title='"+response[i].desk_id+"' onclick='historyPopUp(id)' src='images/unavailableDesk.png' height='70px' width='70px'>";
                            }
                        }
                     }

                output += "</td>"

                if(response[i].section != "GA"){
                    if(response[i].desk_column==3 && response[i+1].desk_row==1 && aisle_inserted==false){
                          output+="<tr class='blank_row'><td colspan='3'></td> </tr>";
                                    var aisle_inserted = true;
                    }
                 }


		    }

			output += "</tr></Table>";

			$("#getDesks").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}



// displays image with highlighted building section when area is selected
function showBuildingImage(id){

   if(id.indexOf('A')>-1){
    	var output="<img class='floor' src='images/ABlock.png'>"
    }
    else if(id.indexOf('B')>-1){
        var output="<img class='floor' src='images/BBlock.png'>"
    }
    else if(id.indexOf('C')>-1){
        var output="<img class='floor' src='images/CBlock.png'>"
    }

    $("#getBuildingImage").html(output);

}


// returns location of person in building when searched for
// if response is 0 informs user that the person searched for is not available
function findPerson() {

    var params = {
        firstName : $("#firstName").val(),
        lastName : $("#lastName").val(),
    }

	$.ajax({
		url : "/desk/seatiFindPerson",
		data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
            var output = "<Table>";

            if(response.length > 0){

                for (var i=0; i<response.length; i++){
                    output +="<div>"+response[i].firstName+" ("+response[i].soeId+") is at desk " + response[i].deskID +", "+response[i].section+"</div>";

                }
            } else {
                output +="<tr> The person you have searched for has not occupied a seat in the system. </tr>";
            }

			output += "</Table>";

			$("#getPerson").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

// provides statistical data to user on when more desks may become available
function getBetterTime() {

	$.ajax({
		url : "/stats/suggestBetterTime",
		//dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		    var output ="<Table><tr><th>Data from last 5 days suggests more desks will be available between "+response+"</th></tr>";

            output += "</Table>";



			$("#getBetterTime").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

// returns pop up box with historical information when a desk is selected
function historyPopUp(id){

var params = {
        deskID : id,
    }

    showCoords(event);

$.ajax({
		url : "/desk/getDeskHistory",
		data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		if (response.length >= 1){

            var output="<div id='desk_history'  class='desk_history' style='position:absolute; top:"+window.y+"px; left:"+window.x+"px'>";

                    output+="<div><b>Desk "+response[0].deskID+" - Occupant History</b></div>";

            		    for (var i=0; i<response.length; i++){

            		        output+=" <div> " + response[i].date+ "        " + response[i].firstName+" "+ response[i].lastName+"</div>";
            		    }

                        output += "</div>";

                        window.addEventListener('mouseup', function(event){
                             var box = document.getElementById('desk_history');
                                   if(event.target != box){
                                         box.style.display = 'none';
                                      }
                               });

			$("#getDeskHistory").html(output);
			}


		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

// provides co-ordinates for onclick popup in historyPopUp
function showCoords(event) {
   window.x = event.clientX;
   window.y = event.clientY;
}

// returns all neighbourhoods in drop down box
function getNeighbourhoodDropdown() {

	$.ajax({
		url : "/admin/getNeighbourhoods",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		    var output ="<select id='neighbourSelect' onchange='getDesks(this.value)'>";

		        output +="<option disabled selected value> --Neighbourhoods--</option>";

			        for (var i=0; i<response.length; i++){

                       output += "<option value='"+response[i].section+"' id='"+response[i].id+"'>"+response[i].name+" - Section "+response[i].section+"</option>";

			        }
            output += "</select>";

			$("#getNeighbourhoodDropdown").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

//displays all notifications
function getMainNotifications() {

	$.ajax({
		url : "/admin/getNotifications",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

		    var output = "<Table><tr>";

            for (var i=0; i<response.length; i++){

                output+="<td>"+response[i].notification+"</td></tr>";

                     }
                 output+="</Table>";

            			$("#getMainNotifications").html(output);
		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

// displays capacity of each section, coloured to indicate how busy a section is
function getCapacityPerSectionMain() {

	$.ajax({
		url : "/stats/getCapacityPerSection",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		    var output ="<Table class='capacityTable'><tr>";

            for (var i = 0, keys = Object.keys(response), ii = keys.length; i < ii; i++) {

                    if(response[keys[i]]>90){
                    output+="<td tabindex='1' class='red' id="+keys[i]+" onclick='getDesks(id)' title='"+ Number(response[keys[i]]).toFixed(0)+"%'>"
                    }
                    else if(response[keys[i]]>70){
                    output+="<td  tabindex='1' class='orange' id="+keys[i]+"  onclick='getDesks(id)' title='"+ Number(response[keys[i]]).toFixed(0)+"%'>"
                    }
                    else{
                     output+="<td tabindex='1' class='green' id="+keys[i]+"  onclick='getDesks(id)' title='"+ Number(response[keys[i]]).toFixed(0)+"%'>"
                    }
                    output+=  keys[i] +"</td>";
            }

            output += "</tr></Table>";

			$("#getCapacityPerSectionMain").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}






