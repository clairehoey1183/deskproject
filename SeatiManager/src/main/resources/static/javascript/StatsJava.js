
$(document).ready(startup);

function startup(){

	getCapacityPerSection();
	getBuildingCapacity();
	getBuildingCapacityByDate();
	getNumberOfPeopleInBuilding();
	getNumberOfDesksInBuilding();
    getNumberOfEmployees();
    getCountOfAvailableDesks();
	}

// displays overall building capacity
function getBuildingCapacity() {

	$.ajax({
		url : "/stats/getCapacity",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		    var output= "<div class='spacer20'></div>"
		            output+= "<div class='box_number'>"+Number(response).toFixed(0)+"%</div>";
                      output+= "<div class='spacer30'></div>"
		                 output+="<div class='chart2'>";
                            output+="<div class='bar2' style='width:"+Number(response).toFixed(0)+"%;background-color:green;'></div>";
                              output+="</div>";

			$("#getBuildingCapacity").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});
}

// displays number of employees in the building at current time
function getNumberOfPeopleInBuilding() {

	$.ajax({
		url : "/stats/capacityCountOccupied",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		   var output= "<div class='spacer20'></div>"
		      output += "<div class='box_number'>"+response+"</div>";
		        var percentage_value= (response/2072)*100;
                    output+= "<div class='spacer30'></div>"
            		  output+="<div class='chart2'>";
                        output+="<div class='bar2' style='width:"+percentage_value+"%;background-color:green;'></div>";
                          output+="</div>";

			$("#getNumberPeopleInBuilding").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});
}

// displays number of desks assigned to mapping system
function getNumberOfDesksInBuilding() {

	$.ajax({
		url : "/stats/capacityCountDesks",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		    var output= "<div class='spacer20'></div>"
              output +="<div class='box_number'>"+response+"</div>";
                var percentage_value= (response/2160)*100;
                    output+= "<div class='spacer30'></div>"
                      output+="<div class='chart2'>";
                        output+="<div class='bar2' style='width:"+percentage_value+"%;background-color:green;'></div>";
                          output+="</div>";
                             output+= "<div class='spacer10'></div>"
                              output+="<div class='bar2_comment'>Current desks vs Maximum Capacity</div>";

			$("#getNumberDesksInBuilding").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

// displays number of employees overall in system
function getNumberOfEmployees() {

	$.ajax({
		url : "/stats/countOfEmployees",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		    var output= "<div class='spacer20'></div>"
               output += "<div class='box_number'>"+response+"</div>";
                   var percentage_value= (response/2072)*100;
                        output+= "<div class='spacer30'></div>"
                           output+="<div class='chart2'>";
                              output+="<div class='bar2' style='width:"+percentage_value+"%;background-color:green;'></div>";
                                output+="</div>";
                                  output+= "<div class='spacer10'></div>"
                                    output+="<div class='bar2_comment'>No. of Employees vs Desks in Building</div>";

			$("#getEmployeeCount").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});
}

// displays count of all available desks within mapping system
function getCountOfAvailableDesks() {

	$.ajax({
		url : "/stats/countOfAvailableDesks",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		    var output = "<div class='spacer20'></div>"
		     output+= "<div class='box_number'>"+response+"</div>";
		       var percentage_value= (response/2072)*100;
                 output+= "<div class='spacer30'></div>"
            		output+="<div class='chart2'>";
                      output+="<div class='bar2' style='width:"+percentage_value+"%;background-color:green;'></div>";
                        output+="</div>";

			$("#getAvailableDeskCount").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}

// displays bar chart of building capacity by date
function getBuildingCapacityByDate() {

	$.ajax({
		url : "/stats/capacityByDate",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {

        for (var i=0; i<response.length; i++){

        var output="<div class='chart'>";
            output+= "<div class=axis_title>"+response[0].capacityDate+"</div>";
            output+="<div class='bar' style='width:"+response[0].capacity+"%;background-color:#001a4d;'>"+response[0].capacity+"%</div>";
            output+= "<div class='spacer10'></div>"
            output+="</div>";

            output+="<div class='chart'>";
            output+= "<div class=axis_title>"+response[1].capacityDate+"</div>";
            output+="<div class='bar' style='width:"+response[1].capacity+"%;background-color:#001a4d;'>"+response[1].capacity+"%</div>";
            output+= "<div class='spacer10'></div>"
            output+="</div>";

            output+="<div class='chart'>";
            output+= "<div class=axis_title>"+response[2].capacityDate+"</div>";
            output+="<div class='bar' style='width:"+response[2].capacity+"%;background-color:#001a4d;'>"+response[2].capacity+"%</div>";
            output+= "<div class='spacer10'></div>"
            output+="</div>";

            output+="<div class='chart'>";
            output+= "<div class=axis_title>"+response[3].capacityDate+"</div>";
            output+="<div class='bar' style='width:"+response[3].capacity+"%;background-color:#001a4d;'>"+response[3].capacity+"%</div>";
            output+= "<div class='spacer10'></div>"
            output+="</div>";

            output+="<div class='chart'>";
            output+= "<div class=axis_title>"+response[4].capacityDate+"</div>";
            output+="<div class='bar' style='width:"+response[4].capacity+"%;background-color:#001a4d;'>"+response[4].capacity+"%</div>";
            output+= "<div class='spacer10'></div>"
            output+="</div>";

            output+= "<div class='spacer20'></div>"

            }


			$("#getCapacityByDate").html(output);
			},
		error : function(err) {
			console.log("Error: "+err.responseText)

             var output = "Not enough values in database to populate chart";

              $("#capacityGraphFailure").html(output);
		}
	});

}

// displays capacity of each section, coloured to indicate how busy a section is
function getCapacityPerSection() {

	$.ajax({
		url : "/stats/getCapacityPerSection",
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
		    var output ="<Table class='capacityTable'><tr>";

            var previous_key="1";

            for (var i = 0, keys = Object.keys(response), ii = keys.length; i < ii; i++) {

                if(!keys[i].includes(previous_key))
                                {
                                    output += "</tr><tr>";
                                    previous_key=keys[i].charAt(0);
                                }

                    if(response[keys[i]]>90){
                    output+="<td tabindex='1' class='red' id="+keys[i]+" onclick='getDesks(id)' title='"+ Number(response[keys[i]]).toFixed(0)+"%'>"
                    }
                    else if(response[keys[i]]>70){
                    output+="<td  tabindex='1' class='orange' id="+keys[i]+"  onclick='getDesks(id)' title='"+ Number(response[keys[i]]).toFixed(0)+"%'>"
                    }
                    else{
                     output+="<td tabindex='1' class='green' id="+keys[i]+"  onclick='getDesks(id)' title='"+ Number(response[keys[i]]).toFixed(0)+"%'>"
                    }
                    output+=  keys[i] +"</td>";
            }

            output += "</tr></Table>";

			$("#getCapacityPerSection").html(output);

		},
		error : function(err) {
			alert("Error: " + err.responseText);
		}
	});

}






