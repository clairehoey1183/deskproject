package javaTests.businessTest;

import GatewayManager.business.AdminBS;
import GatewayManager.dao.AdminDao;
import GatewayManager.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdminBsTest {

    @InjectMocks
    AdminBS business;

    @Mock
    AdminDao dao;

    @Test
    public void getAllEmployeesTest(){
        List<Employee> empList = new ArrayList<Employee>();
        Employee emp1 = new Employee("1","Joey","Bun");
        Employee emp2 = new Employee("2","Simon","Bates");
        Employee emp3 = new Employee("3","Jimmy","Neutron");

        empList.add(emp1);
        empList.add(emp2);
        empList.add(emp3);

        when(dao.getAllEmployees()).thenReturn(empList);

        List<Employee> employeeList = business.getAllEmployees();

        assertEquals(3, employeeList.size());
        verify(dao, times(1)).getAllEmployees();
    }

    @Test
    public void getAllAdminUsersTest(){
        List<AdminUser> adminList = new ArrayList<>();
        AdminUser admin1 = new AdminUser("admin1","pass1");
        AdminUser admin2 = new AdminUser("admin2","pass2");
        AdminUser admin3 = new AdminUser("admin3","pass3");

        adminList.add(admin1);
        adminList.add(admin2);
        adminList.add(admin3);

        when(dao.getAllAdminUsers()).thenReturn(adminList);

        List<AdminUser> adminUserList = business.getAllAdminUsers();

        assertEquals(3, adminUserList.size());
        verify(dao, times(1)).getAllAdminUsers();
    }

    @Test
    public void getAllUnassignedDesksTest(){
        List<UnassignedDesk> unassignedDesks = new ArrayList<>();
        UnassignedDesk desk1 = new UnassignedDesk(1,1);
        UnassignedDesk desk2 = new UnassignedDesk(2,2);

        unassignedDesks.add(desk1);
        unassignedDesks.add(desk2);

        when(dao.getAllUnassignedDesks()).thenReturn(unassignedDesks);

        List<UnassignedDesk> newUnassignedDeskList = business.getAllUnassignedDesks();

        assertEquals(2, newUnassignedDeskList.size());
        verify(dao, times(1)).getAllUnassignedDesks();

    }

    @Test
    public void addDeskTest(){
        AllDesksAndOccupied desk = new AllDesksAndOccupied(1,1,1,1,"GA",1);

        business.addDesk(desk);

        verify(dao, times(1)).addDesk(desk);
    }

    @Test
    public void addAdminTest(){
        AdminUser admin = new AdminUser("admin","password");

        business.addAdmin(admin);

        verify(dao, times(1)).addAdmin(admin);
    }

    @Test
    public void removeDeskTest(){
        business.removeDesk(1);

        verify(dao,times(1)).removeDesk(1);
    }

    @Test
    public void removeEmployeeTest(){
        business.removeEmployee("1");

        verify(dao,times(1)).removeEmployee("1");
    }

    @Test
    public void removeAdminUserTest(){
        business.removeAdminUser("admin");

        verify(dao, times(1)).removeAdminUser("admin");
    }

    @Test
    public void getNotificationsTest(){
        List<Notification> note = new ArrayList<Notification>();
        Notification note1 = new Notification(1,"Test Notification 1");
        Notification note2 = new Notification(2,"Test Notification 2");
        Notification note3 = new Notification(3,"Test Notification 3");

        note.add(note1);
        note.add(note2);
        note.add(note3);

        when(dao.getAllNotifications()).thenReturn(note);

        List<Notification> noteList = business.getNotifications();
        assertEquals(3,noteList.size());
        verify(dao, times(1)).getAllNotifications();
    }

    @Test
    public void getNeighbourhoodsTest(){
        List<Neighbour>neighbour = new ArrayList<Neighbour>();
        Neighbour neighbour1 = new Neighbour("1","Neighbour1","GA");
        Neighbour neighbour2 = new Neighbour("2", "Neighbour2", "GB");

        neighbour.add(neighbour1);
        neighbour.add(neighbour2);

        when(dao.getNeighbourhoods()).thenReturn(neighbour);
        List<Neighbour> neighbourList = business.getNeighbourhoods();
        assertEquals(2,neighbourList.size());
        verify(dao,times(1)).getNeighbourhoods();
    }

    @Test
    public void addNotificationTest(){
        Notification note = new Notification(1,"Notification test");

        business.addNotification(note);

        verify(dao,times(1)).addNotification(note);
    }

    @Test
    public void removeNoteTest(){
        business.removeNotification(1);

        verify(dao,times(1)).removeNotification(1);
    }

    @Test
    public void changeNeighbourhoodTest(){
        Neighbour neighbour = new Neighbour();
        business.changeNeighbourhood(neighbour);

        verify(dao,times(1)).changeNeighbourhood(neighbour);
    }

    @Test
    public void changeNotificationTest(){
        Notification note = new Notification();
        business.changeNotification(note);

        verify(dao,times(1)).updateNotification(note);
    }
}
