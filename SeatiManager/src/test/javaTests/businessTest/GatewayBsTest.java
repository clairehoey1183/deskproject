package javaTests.businessTest;

import GatewayManager.business.GatewayBS;
import GatewayManager.dao.GatewayDao;
import GatewayManager.dto.AllDesksAndOccupied;
import GatewayManager.dto.DeskByNeighbourhood;
import GatewayManager.dto.OccupantHistory;
import GatewayManager.dto.OccupiedDesk;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GatewayBsTest {

    @InjectMocks
    GatewayBS business;

    @Mock
    GatewayDao gatewayDao;

    @Test
    public void returnAllOccupiedAndAvailableDesksTest(){
        List<AllDesksAndOccupied> deskList = new ArrayList<AllDesksAndOccupied>();
        AllDesksAndOccupied desk = new AllDesksAndOccupied(1,1,1,1,"GA",1);
        deskList.add(desk);
        when(business.findOccupiedDesks("1")).thenReturn(deskList);
        assertEquals(deskList, business.findOccupiedDesks("1"));
        verify(gatewayDao,times(1)).findOccupiedDesks("1");
    }


    @Test
    public void findPersonTest(){
        Date date = new Date(100000);
        OccupiedDesk deskOccupant = new OccupiedDesk(1,"firstName","lastName","test01","GA", date);
        List<OccupiedDesk> deskList = new ArrayList<>();
        deskList.add(deskOccupant);
        when(business.findPerson("firstName", "lastName")).thenReturn(deskList);
        assertEquals(deskList, business.findPerson("firstName","lastName"));
        verify(gatewayDao,times(1)).findPerson("firstName","lastName");
    }

    @Test
    public void getDesksByNeighbourhoodTest(){
        DeskByNeighbourhood desk = new DeskByNeighbourhood(1,1,"GA");
        business.getDesksByNeighbourhood(1);
        verify(gatewayDao, times(1)).getDesksByNeighbourhood(1);
    }

    @Test
    public void getDeskHistoryTest(){
        Date date = new Date(100000);
        OccupantHistory occupant = new OccupantHistory(1,"firstName", "lastName", "soe01", date);
        business.getDeskHistory(1);
        verify(gatewayDao, times(1)).getDeskHistory(1);
    }
}
