package javaTests.businessTest;

import GatewayManager.business.StatsBS;
import GatewayManager.dao.StatsDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StatsBsTest {

    @InjectMocks
    StatsBS business;

    @Mock
    StatsDao statsDao;

    @Before
    public void setUp(){
        when(statsDao.getCapacityCountOccupied()).thenReturn(1.0);
        when(statsDao.getCapacityCountDesks()).thenReturn(1.0);
        when(statsDao.storeCapacityStats(anyDouble())).thenReturn(1.0);
    }

    @Test
    public void getSectionsTest(){
        List<String> sections = new ArrayList<>();
        String section1 = "GA";
        String section2 = "GB";
        String section3 = "GC";

        sections.add(section1);
        sections.add(section2);
        sections.add(section3);

        when(statsDao.getSections()).thenReturn(sections);
        List<String>sectionList = business.getSections();
        assertEquals(3,sectionList.size());
        verify(statsDao, times(1)).getSections();
    }

    @Test
    public void getCapacityTest(){
        double capacityExpected = 100.0;

        assertEquals(capacityExpected, business.getCapacityCount(), 0);
        verify(statsDao,times(1)).getCapacityCountDesks();
        verify(statsDao,times(1)).getCapacityCountOccupied();
        verify(statsDao,times(1)).storeCapacityStats(anyDouble());
    }

    @Test
    public void getCapacityPerSectionTest(){
        //statsDao.getCapacityPerSection();
        business.getCapacityPerSection();
        verify(statsDao, times(1)).getCapacityPerSection();
    }


    @Test
    public void averageCapacityByDateTest(){
        business.averageCapacityByDate();
        verify(statsDao,times(1)).averageCapacityByDate();
    }

    @Test
    public void capacityCountOccupiedTest(){
        business.getCapacityCountOccupied();
        verify(statsDao,times(1)).getCapacityCountOccupied();
    }

    @Test
    public void capacityCountDesksTest(){
        business.getCapacityCountDesks();
        verify(statsDao, times(1)).getCapacityCountDesks();
    }

    @Test
    public void countOfEmployeesTest(){
        business.getCountOfEmployees();
        verify(statsDao,times(1)).getCountOfEmployees();
    }



}
