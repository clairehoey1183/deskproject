package javaTests.daoTest;

import GatewayManager.dao.StatsDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StatsDaoTest {

    @InjectMocks
    StatsDao statsDao;

    @Mock
    JdbcTemplate jdbcTemplateMock;

    @Test
    public void testGetCapacityCountDesk(){
        double count = 1;
        when(jdbcTemplateMock.queryForObject("SELECT COUNT(deskID) FROM desk_map", Double.class)).thenReturn(count);
        assertEquals(count, statsDao.getCapacityCountDesks(), 0);
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT COUNT(deskID) FROM desk_map", Double.class);
    }

    @Test
    public void testGetCapacityCountOccupied(){
        double count =1;
        when(jdbcTemplateMock.queryForObject("SELECT COUNT(id) FROM occupied", Double.class)).thenReturn(count);
        assertEquals(count, statsDao.getCapacityCountOccupied(),0 );
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT COUNT(id) FROM occupied", Double.class);
    }

    @Test
    public void testGetCountOfEmployees(){
        double count =1;
        when(jdbcTemplateMock.queryForObject("SELECT COUNT(SOEid) FROM employees", Double.class)).thenReturn(count);
        assertEquals(count, statsDao.getCountOfEmployees(),0 );
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT COUNT(SOEid) FROM employees", Double.class);
    }

    @Test
    public void testGetCountForEachSection(){
        List<String> keyList = new ArrayList<>();
        keyList.add("key");

        Map<String, Double> countOfDeskPerSection = new HashMap<>();
        countOfDeskPerSection.put("key", 1.0);
        Double count = 1.0;

        when(jdbcTemplateMock.queryForList("SELECT DISTINCT section FROM desk_map;", String.class)).thenReturn(keyList);
        when(jdbcTemplateMock.queryForObject("SELECT COUNT(deskID) FROM desk_map WHERE section='" + keyList.get(0)+"'", Double.class)).thenReturn(count);

        assertEquals(countOfDeskPerSection, statsDao.getCountForEachSection());
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT COUNT(deskID) FROM desk_map WHERE section='" + keyList.get(0)+"'", Double.class);
        verify(jdbcTemplateMock,times(1)).queryForList("SELECT DISTINCT section FROM desk_map;", String.class);
    }

    @Test
    public void testGetCapacityPerSection(){
        List<String> keyList = new ArrayList<>();
        keyList.add("key");

        Map<String, Double> capacityOfDeskPerSection = new HashMap<>();
        capacityOfDeskPerSection.put("key", 1.0);
        Double count = 1.0;

        when(jdbcTemplateMock.queryForList("SELECT DISTINCT section FROM desk_map;", String.class)).thenReturn(keyList);
        when(jdbcTemplateMock.queryForObject("SELECT COUNT(desks.deskid) FROM desks INNER JOIN desk_map ON desks.deskid = desk_map.deskID JOIN occupied ON desks.assetNo = occupied.assetNo  WHERE section = '" + keyList.get(0)+"'", Double.class)).thenReturn(count);

        assertEquals(capacityOfDeskPerSection, statsDao.getCapacityPerSection());
        verify(jdbcTemplateMock, times(1)).queryForObject("SELECT COUNT(desks.deskid) FROM desks INNER JOIN desk_map ON desks.deskid = desk_map.deskID JOIN occupied ON desks.assetNo = occupied.assetNo  WHERE section = '" + keyList.get(0)+"'", Double.class);
        verify(jdbcTemplateMock, times(1)).queryForList("SELECT DISTINCT section FROM desk_map;", String.class);
    }

    @Test
    public void testStoreCapacityStats(){
        double capacity = 1;
        when(jdbcTemplateMock.update("INSERT INTO capacity_stats (capacity, time) VALUES (?, NOW())", capacity)).thenReturn(1);
        assertEquals(capacity,statsDao.storeCapacityStats(capacity),0);
        verify(jdbcTemplateMock, times(1)).update("INSERT INTO capacity_stats (capacity, time) VALUES (?, NOW())", capacity);
    }

    @Test
    public void testDeleteCapacityStatsOnFixedBasis(){
        when(jdbcTemplateMock.update("DELETE from capacity_stats WHERE time < NOW() - INTERVAL "+5+" DAY")).thenReturn(1);
        assertEquals(0, statsDao.deleteCapacityStatsOnFixedBasis(), 0);
        verify(jdbcTemplateMock,times(1)).update("DELETE from capacity_stats WHERE time < NOW() - INTERVAL "+5+" DAY");
    }

    @Test
    public void testSuggestBetterTimeBasedOnCapacity(){
        Map<String, Integer> timings = new HashMap<>();

        timings.put("07.00 - 10.00", 1);
        timings.put("10.00 - 12.00", 2);
        timings.put("12.00 - 14.00", 3);
        timings.put("14.00 - 17.00", 4);

        when(jdbcTemplateMock.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '7' and '10';", Integer.class)).thenReturn(1);
        when(jdbcTemplateMock.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '10' and '12';", Integer.class)).thenReturn(2);
        when(jdbcTemplateMock.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '12' and '14';", Integer.class)).thenReturn(3);
        when(jdbcTemplateMock.queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '14' and '17';", Integer.class)).thenReturn(4);

        assertEquals(timings, statsDao.suggestBetterTimeBasedOnCapacity());
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '7' and '10';", Integer.class);
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '10' and '12';", Integer.class);
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '12' and '14';", Integer.class);
        verify(jdbcTemplateMock,times(1)).queryForObject("SELECT AVG(capacity) FROM capacity_stats where HOUR (time) between '14' and '17';", Integer.class);
    }



}
