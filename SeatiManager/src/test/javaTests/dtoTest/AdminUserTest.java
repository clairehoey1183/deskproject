package javaTests.dtoTest;

import GatewayManager.dto.AdminUser;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AdminUserTest {

    AdminUser adminUser;

    @Before
    public void setup(){
        adminUser = new AdminUser("username", "password");
    }

    @Test
    public void testDefaultConstructor(){
        adminUser = new AdminUser();
        assertNotNull(adminUser);
    }

    @Test
    public void testToString(){
        String expected = "AdminUser{" +
                "userName='" + "username" + '\'' +
                ", passw='" + "password" + '\'' +
                '}';
        String actual = adminUser.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testConstructorSetup(){
        String userName = "username";
        String password = "password";

        assertEquals(userName, adminUser.getUserName());
        assertEquals(password, adminUser.getPassw());
    }

    @Test
    public void usernameGetterAndSetter(){
        String expected = "username";
        adminUser.setUserName(expected);
        String actual = adminUser.getUserName();
        assertEquals(expected, actual);
    }

    @Test
    public void passwordGetterAndSetter(){
        String expected = "password";
        adminUser.setPassw(expected);
        String actual = adminUser.getPassw();
        assertEquals(expected, actual);
    }
}
