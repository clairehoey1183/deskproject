package javaTests.dtoTest;

import GatewayManager.dto.AllDesksAndOccupied;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class AllDesksAndOccupiedTest {

    AllDesksAndOccupied allDesksAndOccupied;

    @Before
    public void setup(){
        //Test constructor in setup
        allDesksAndOccupied = new AllDesksAndOccupied(1, 1,1,1,"GA",1);
    }

    @Test
    public void testDefaultConstructor(){
        allDesksAndOccupied = new AllDesksAndOccupied();
        assertNotNull(allDesksAndOccupied);
    }

    @Test
    public void testConstructorSetup(){
        int deskId, deskColumn, deskRow, occupied, neighbourhoodId;
        deskId = 1;
        deskColumn = 1;
        deskRow = 1;
        occupied = 1;
        neighbourhoodId = 1;
        assertEquals(deskId, allDesksAndOccupied.getDesk_id());
        assertEquals(deskColumn, allDesksAndOccupied.getDesk_column());
        assertEquals(deskRow, allDesksAndOccupied.getDesk_row());
        assertEquals(occupied, allDesksAndOccupied.getOccupied());
        assertEquals(neighbourhoodId, allDesksAndOccupied.getNeighbourhoodId());
    }

    @Test
    public void deskidGetterAndSetter(){
        int expected = 1234;
        allDesksAndOccupied.setDesk_id(expected);
        int actual = allDesksAndOccupied.getDesk_id();
        assertEquals(expected, actual);
    }

    @Test
    public void deskColumnGetterAndSetter(){
        int expected = 1;
        allDesksAndOccupied.setDesk_column(expected);
        int actual = allDesksAndOccupied.getDesk_column();
        assertEquals(expected,actual);
    }

    @Test
    public void deskRowGetterAndSetter(){
        int expected=1;
        allDesksAndOccupied.setDesk_row(expected);
        int actual = allDesksAndOccupied.getDesk_row();
        assertEquals(expected, actual);
    }

    @Test
    public void occupiedGetterAndSetter(){
        int expected = 1;
        allDesksAndOccupied.setOccupied(expected);
        int actual = allDesksAndOccupied.getOccupied();
        assertEquals(expected, actual);
    }

    @Test
    public void neighbourhoodIdGetterAndSetter(){
        int expected = 1;
        allDesksAndOccupied.setNeighbourhoodId(expected);
        int actual = allDesksAndOccupied.getNeighbourhoodId();
        assertEquals(expected, actual);
    }

    @Test
    public void sectionGetterAndSetter(){
        String expected = "GA";
        allDesksAndOccupied.setSection(expected);
        String actual = allDesksAndOccupied.getSection();
        assertEquals(expected, actual);
    }

    @Test
    public void testToString(){
        String expected = "AllDesksAndOccupied{" +
                "desk_id=" + 1 +
                ", desk_column=" + 1 +
                ", desk_row=" + 1 +
                ", occupied=" + 1 +
                ", neighbourhoodId=" + 1 +
                ", section='" + "GA" + '\'' +
                '}';
        String actual = allDesksAndOccupied.toString();
        assertEquals(expected,actual);
    }
}



