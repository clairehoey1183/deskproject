package javaTests.dtoTest;

import GatewayManager.dto.AvailableDesk;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AvailableDeskTest {

    AvailableDesk availableDesk;

    @Before
    public void setup(){
        availableDesk = new AvailableDesk(1);
    }

    @Test
    public void testDefaultConstructor(){
        availableDesk = new AvailableDesk();
        assertNotNull(availableDesk);
    }

    @Test
    public void testConstructor(){
        int deskID = 1;
        assertEquals(deskID, availableDesk.getDeskID());
    }


    @Test
    public void deskIdGetterAndSetter(){
        int expected = 1;
        availableDesk.setDeskID(expected);
        int actual = availableDesk.getDeskID();
        assertEquals(expected,actual);
    }
}
