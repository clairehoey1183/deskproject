package javaTests.dtoTest;

import GatewayManager.dto.CapacityMap;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.assertEquals;

public class CapacityMapTest {

    CapacityMap capacityMap;

    @Before
    public void setup(){
        capacityMap = new CapacityMap("date", 10);
    }

    @Test
    public void testConstructorSetup(){
        String date = "date";
        int capacity = 10;

        assertEquals(date, capacityMap.getCapacityDate());
        assertEquals(capacity, capacityMap.getCapacity());
    }

    @Test
    public void dateGetterAndSetter(){
        String expected = "date";
        capacityMap.setCapacityDate(expected);
        String actual = capacityMap.getCapacityDate();
        assertEquals(expected, actual);
    }

    @Test
    public void capacityGetterAndSetter(){
        int expected = 10;
        capacityMap.setCapacity(expected);
        int actual = capacityMap.getCapacity();
        assertEquals(expected, actual);
    }

    @Test
    public void testToString(){
        Date date = new Date(10000);
        String expected = "CapacityMap{" +
                "capacityDate='" + "date" + '\'' +
                ", capacity=" + 10 +
                '}';

        String actual = capacityMap.toString();
        assertEquals(expected,actual);
    }
}
