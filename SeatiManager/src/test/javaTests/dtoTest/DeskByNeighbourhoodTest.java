package javaTests.dtoTest;

import GatewayManager.dto.DeskByNeighbourhood;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DeskByNeighbourhoodTest {

    DeskByNeighbourhood deskByNeighbourhood;

    @Before
    public void setup(){
        deskByNeighbourhood = new DeskByNeighbourhood(1,1,"GA");
    }

    @Test
    public void testConstructorSetup(){
        int deskID = 1;
        int neighbourhoodId = 1;
        String section = "GA";

        assertEquals(deskID, deskByNeighbourhood.getDeskID());
        assertEquals(neighbourhoodId, deskByNeighbourhood.getNeighbourhoodId());
        assertEquals(section, deskByNeighbourhood.getSection());
    }

    @Test
    public void deskIdGetterAndSetter(){
        int expected = 1;
        deskByNeighbourhood.setDeskID(expected);
        int actual = deskByNeighbourhood.getDeskID();
        assertEquals(expected, actual);
    }

    @Test
    public void neighbourhoodIdGetterAndSetter(){
        int expected = 1;
        deskByNeighbourhood.setNeighbourhoodId(expected);
        int actual = deskByNeighbourhood.getNeighbourhoodId();
        assertEquals(expected, actual);
    }

    @Test
    public void sectionGetterAndSetter(){
        String expected = "GA";
        deskByNeighbourhood.setSection(expected);
        String actual = deskByNeighbourhood.getSection();
        assertEquals(expected, actual);
    }

    @Test
    public void testToString(){
        String expected = "DeskByNeighbourhood{" +
                "deskID=" + 1 +
                ", neighbourhoodId=" + 1 +
                ", section='" + "GA" + '\'' +
                '}';
        String actual = deskByNeighbourhood.toString();
        assertEquals(expected, actual);
    }
}
