package javaTests.dtoTest;

import GatewayManager.dto.Employee;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EmployeeTest {

    Employee employee;

    @Before
    public void setup(){
        employee = new Employee("choey01", "claire", "hoey");
    }


    @Test
    public void testDefaultConstructor(){
        employee = new Employee();
        assertNotNull(employee);
    }

    @Test
    public void testConstructorSetup(){
        String soeID = "choey01";
        String firstName = "claire";
        String lastName = "hoey";

        assertEquals(soeID, employee.getSoeID());
        assertEquals(firstName, employee.getFirstName());
        assertEquals(lastName, employee.getLastName());
    }

    @Test
    public void soeIdGetterAndSetter(){
        String expected = "choey01";
        employee.setSoeID(expected);
        String actual = employee.getSoeID();
        assertEquals(expected, actual);
    }

    @Test
    public void firstNameGetterAndSetter(){
        String expected = "claire";
        employee.setFirstName(expected);
        String actual = employee.getFirstName();
        assertEquals(expected, actual);
    }

    @Test
    public void lastNameGetterAndSetter(){
        String expected = "hoey";
        employee.setLastName(expected);
        String actual = employee.getLastName();
        assertEquals(expected, actual);
    }

    @Test
    public void testToString(){
        String expected = "Employee{" +
                "soeID=" + "choey01" +
                ", firstName='" + "claire" + '\'' +
                ", lastName='" + "hoey" + '\'' +
                '}';
        String actual = employee.toString();
        assertEquals(expected,actual);
    }
}
