package javaTests.dtoTest;

import GatewayManager.dto.Neighbour;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NeighbourTest {

    Neighbour neighbour;

    @Before
    public void setup(){
        neighbour = new Neighbour("1","FX","GA");
    }

    @Test
    public void testConstructorSetup(){
        String id = "1";
        String name = "FX";
        String section = "GA";

        assertEquals(id, neighbour.getId());
        assertEquals(name, neighbour.getName());
        assertEquals(section, neighbour.getSection());
    }

    @Test
    public void idGetterAndSetter(){
        String expected = "1";
        neighbour.setId(expected);
        String actual = neighbour.getId();
        assertEquals(expected, actual);
    }

    @Test
    public void nameGetterAndSetter(){
        String expected = "FX";
        neighbour.setName(expected);
        String actual = neighbour.getName();
        assertEquals(expected, actual);
    }

    @Test
    public void sectionGetterAndSetter(){
        String expected = "GA";
        neighbour.setSection(expected);
        String actual = neighbour.getSection();
        assertEquals(expected, actual);
    }

    @Test
    public void testToString(){
        String expected = "Neighbour{" +
                "id=" + 1 +
                ", name='" + "FX" + '\'' +
                ", section='" + "GA" + '\'' +
                '}';
        String actual = neighbour.toString();
        assertEquals(expected, actual);
    }
}
