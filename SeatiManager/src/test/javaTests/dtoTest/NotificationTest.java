package javaTests.dtoTest;

import GatewayManager.dto.Notification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class NotificationTest {

    Notification notification1;

    @Before
    public void setup(){
        notification1 = new Notification(1,"notificationTest");
    }

    @Test
    public void testConstructorSetup(){
        int id = 1;
        String notification = "notificationTest";

        assertEquals(id,notification1.getId());
        assertEquals(notification, notification1.getNotification());
    }

    @Test
    public void idGetterAndSetter(){
        int expected = 1;
        notification1.setId(expected);
        int actual = notification1.getId();
        assertEquals(expected, actual);
    }

    @Test
    public void notificationGetterAndSetter(){
        String expected = "notificationTest";
        notification1.setNotification(expected);
        String actual = notification1.getNotification();
        assertEquals(expected, actual);
    }
}
