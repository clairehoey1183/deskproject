package javaTests.dtoTest;

import GatewayManager.dto.AllDesksAndOccupied;
import GatewayManager.dto.OccupantHistory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class OccupantHistoryTest {

    OccupantHistory occupantHistory;

    @Before
    public void setup(){
        Date date = new Date(10000);
        //Test constructor in setup
        occupantHistory = new OccupantHistory(1, "firstName","lastName","1",date);
    }

    @Test
    public void testConstructorSetup(){
        int deskId;
        String firstName, lastName, soeId;
        deskId = 1;
        firstName = "firstName";
        lastName = "lastName";
        soeId = "1";
        assertEquals(deskId, occupantHistory.getDeskID());
        assertEquals(firstName, occupantHistory.getFirstName());
        assertEquals(lastName, occupantHistory.getLastName());
        assertEquals(soeId, occupantHistory.getSoeId());
    }

    @Test
    public void deskIdGetterAndSetter(){
        int expected = 1;
        occupantHistory.setDeskID(expected);
        int actual = occupantHistory.getDeskID();
        assertEquals(expected,actual);
    }

    @Test
    public void firstNameGetterAndSetter(){
        String expected = "firstName";
        occupantHistory.setFirstName(expected);
        String actual = occupantHistory.getFirstName();
        assertEquals(expected,actual);
    }

    @Test
    public void lastNameGetterAndSetter(){
        String expected = "lastName";
        occupantHistory.setLastName(expected);
        String actual = occupantHistory.getLastName();
        assertEquals(expected,actual);
    }
    @Test
    public void soeIdGetterAndSetter(){
        String expected = "1";
        occupantHistory.setSoeId(expected);
        String actual = occupantHistory.getSoeId();
        assertEquals(expected,actual);
    }

    @Test
    public void dateGetterAndSetter(){
        Date date = new Date(1000000);
        Date expected = date;
        occupantHistory.setDate(expected);
        Date actual = occupantHistory.getDate();
        assertEquals(expected,actual);
    }

    @Test
    public void testToString(){
        Date date = new Date(10000);
        String expected = "OccupantHistory{" +
                "deskID=" + 1 +
                ", firstName='" + "firstName" + '\'' +
                ", lastName='" + "lastName" + '\'' +
                ", soeId='" + 1 + '\'' +
                ", date=" + date +
                '}';
        String actual = occupantHistory.toString();
        assertEquals(expected, actual);
    }
}




