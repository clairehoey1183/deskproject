package javaTests.dtoTest;

import GatewayManager.dto.AllDesksAndOccupied;
import GatewayManager.dto.OccupiedDesk;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class OccupiedDeskTest {

    OccupiedDesk occupiedDesk;

    @Before
    public void setup(){
        Date date = new Date(100000);
        //Test constructor in setup
        occupiedDesk = new OccupiedDesk(1, "firstName","lastName","1","GA", date);
    }

    @Test
    public void testConstructorSetup(){
        int deskid;
        String firstName, lastName, soeId, section;
        deskid = 1;
        firstName = "firstName";
        lastName = "lastName";
        soeId = "1";
        section = "GA";

        assertEquals(deskid, occupiedDesk.getDeskID());
        assertEquals(firstName, occupiedDesk.getFirstName());
        assertEquals(lastName, occupiedDesk.getLastName());
        assertEquals(soeId, occupiedDesk.getSoeId());
        assertEquals(section, occupiedDesk.getSection());
    }

    @Test
    public void deskIdGetterAndSetter(){
       int expected = 1;
       occupiedDesk.setDeskID(expected);
       int actual = occupiedDesk.getDeskID();
       assertEquals(expected,actual);
    }

    @Test
    public void firstNameGetterAndSetter(){
        String expected = "firstName";
        occupiedDesk.setFirstName(expected);
        String actual = occupiedDesk.getFirstName();
        assertEquals(expected,actual);
    }

    @Test
    public void lastNameGetterAndSetter(){
        String expected = "lastName";
        occupiedDesk.setLastName(expected);
        String actual = occupiedDesk.getLastName();
        assertEquals(expected,actual);
    }

    @Test
    public void soeIdGetterAndSetter(){
        String expected = "1";
        occupiedDesk.setSoeId(expected);
        String actual = occupiedDesk.getSoeId();
        assertEquals(expected,actual);
    }

    @Test
    public void sectionGetterAndSetter(){
        String expected = "GA";
        occupiedDesk.setSection(expected);
        String actual = occupiedDesk.getSection();
        assertEquals(expected,actual);
    }

    @Test
    public void dateGetterAndSetter(){
        Date date = new Date(1000000);
        Date expected = date;
        occupiedDesk.setDate(expected);
        Date actual = occupiedDesk.getDate();
        assertEquals(expected,actual);
    }

    @Test
    public void testToString(){
        Date date = new Date(100000);
        String expected = "OccupiedDesk{" +
                "deskID=" + 1 +
                ", firstName='" + "firstName" + '\'' +
                ", lastName='" + "lastName" + '\'' +
                ", soeId='" + 1 + '\'' +
                ", section='" + "GA" + '\'' +
                ", date=" + date +
                '}';
        String actual = occupiedDesk.toString();
        assertEquals(expected, actual);
    }
}


