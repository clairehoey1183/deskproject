package javaTests.dtoTest;

import GatewayManager.dto.OccupiedDesk;
import GatewayManager.dto.UnassignedDesk;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UnassignedDeskTest {

    UnassignedDesk unassignedDesk;

    @Before
    public void setup(){
        unassignedDesk = new UnassignedDesk(1,2);
    }

    @Test
    public void testDefaultConstructor(){
        unassignedDesk = new UnassignedDesk();
        assertNotNull(unassignedDesk);
    }

    @Test
    public void testConstructorSetup(){
        int deskId =1;
        int unassignedDeskId = 2;

        assertEquals(deskId, unassignedDesk.getDeskId());
        assertEquals(unassignedDeskId, unassignedDesk.getUnassignedDeskId());
    }

    @Test
    public void deskIdGetterAndSetter(){
        int expected = 1;
        unassignedDesk.setDeskId(expected);
        int actual = unassignedDesk.getDeskId();
        assertEquals(expected,actual);
    }

    @Test
    public void unassignedDeskIdGetterAndSetter(){
        int expected = 2;
        unassignedDesk.setUnassignedDeskId(expected);
        int actual = unassignedDesk.getUnassignedDeskId();
        assertEquals(expected, actual);
    }

    @Test
    public void testToString(){
        String expected = "UnassignedDesk{" +
                "deskId=" + 1 +
                ", unassignedDeskId=" + 2 +
                '}';
        String actual = unassignedDesk.toString();
        assertEquals(expected, actual);
    }
}
