package javaTests.restLayerTest;

import GatewayManager.business.AdminBS;
import GatewayManager.dto.*;
import GatewayManager.rest.AdminController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdminControllerTest {

    @InjectMocks
    AdminController controller;

    @Mock
    AdminBS adminBS;

    @Test
    public void getAllEmployeesTest(){
        List<Employee> empList = new ArrayList<Employee>();
        Employee emp1 = new Employee("1","First","Last");
        Employee emp2 = new Employee("2","First1","Last1");
        Employee emp3 = new Employee("3","First2","Last2");

        empList.add(emp1);
        empList.add(emp2);
        empList.add(emp3);

        when(adminBS.getAllEmployees()).thenReturn(empList);

        List<Employee> employeeList = controller.getAllEmployees();

        assertEquals(3, employeeList.size());
        verify(adminBS, times(1)).getAllEmployees();
    }

    @Test
    public void addDeskTest(){
        AllDesksAndOccupied desk = new AllDesksAndOccupied(1,1,1,1,"GA",1);

        controller.addDesk(desk);

        verify(adminBS, times(1)).addDesk(desk);
    }

    @Test
    public void removeDeskTest(){
        controller.removeDesk(1);

        verify(adminBS,times(1)).removeDesk(1);
    }

    @Test
    public void removeEmployeeTest(){
        controller.removeEmployee("1234");

        verify(adminBS,times(1)).removeEmployee("1234");
    }

    @Test
    public void getNotificationsTest(){
        List<Notification> note = new ArrayList<Notification>();
        Notification note1 = new Notification(1,"Test Notification 1");
        Notification note2 = new Notification(2,"Test Notification 2");
        Notification note3 = new Notification(3,"Test Notification 3");

        note.add(note1);
        note.add(note2);
        note.add(note3);

        when(adminBS.getNotifications()).thenReturn(note);

        List<Notification> noteList = controller.getNotifications();
        assertEquals(3,noteList.size());
        verify(adminBS, times(1)).getNotifications();
    }

    @Test
    public void getNeighbourhoodsTest(){
        List<Neighbour>neighbour = new ArrayList<Neighbour>();
        Neighbour neighbour1 = new Neighbour("1","Neighbour1","GA");
        Neighbour neighbour2 = new Neighbour("2", "Neighbour2", "GB");

        neighbour.add(neighbour1);
        neighbour.add(neighbour2);

        when(adminBS.getNeighbourhoods()).thenReturn(neighbour);
        List<Neighbour> neighbourList = controller.getNeighbourhoods();
        assertEquals(2,neighbourList.size());
        verify(adminBS,times(1)).getNeighbourhoods();
        }

    @Test
    public void getAllAdminUsersTest(){
        List<AdminUser>admin=new ArrayList<>();
        AdminUser admin1 = new AdminUser("admin1", "pass1");
        AdminUser admin2 = new AdminUser("admin2", "pass2");

        admin.add(admin1);
        admin.add(admin2);

        when(adminBS.getAllAdminUsers()).thenReturn(admin);
        List<AdminUser> adminUserList = controller.getAllAdminUsers();
        assertEquals(2,adminUserList.size());
        verify(adminBS, times(1)).getAllAdminUsers();
    }

    @Test
    public void getAllUnassignedDesksTest(){
        List<UnassignedDesk>desk=new ArrayList<>();
        UnassignedDesk desk1 = new UnassignedDesk(1, 1);
        UnassignedDesk desk2 = new UnassignedDesk(2, 2);

        desk.add(desk1);
        desk.add(desk2);

        when(adminBS.getAllUnassignedDesks()).thenReturn(desk);
        List<UnassignedDesk> unassignedDeskList = controller.getAllUnassignedDesks();
        assertEquals(2,unassignedDeskList.size());
        verify(adminBS, times(1)).getAllUnassignedDesks();
    }


     @Test
    public void addNotificationTest(){
        Notification note = new Notification(1,"Notification test");

        controller.addNotification(note);

        verify(adminBS,times(1)).addNotification(note);
     }

     @Test
     public void addAdminTest(){
        AdminUser admin = new AdminUser("admin", "password");

        controller.addAdmin(admin);

        verify(adminBS, times(1)).addAdmin(admin);
     }

     @Test
    public void removeNotificationTest(){
        controller.removeNotification(1);

        verify(adminBS,times(1)).removeNotification(1);
     }

     @Test
     public void removeAdminUserTest(){
        controller.removeAdminUser("admin");
        verify(adminBS, times(1)).removeAdminUser("admin");
     }

     @Test
    public void changeNeighbourhoodTest(){
        Neighbour neighbour = new Neighbour();
        controller.changeNeighbourhood(neighbour);

        verify(adminBS,times(1)).changeNeighbourhood(neighbour);
     }

    @Test
    public void changeNotificationTest(){
        Notification note = new Notification();
        controller.changeNotification(note);

        verify(adminBS,times(1)).changeNotification(note);
    }
}






