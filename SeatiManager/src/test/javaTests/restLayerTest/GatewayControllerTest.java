package javaTests.restLayerTest;

import GatewayManager.business.GatewayBS;
import GatewayManager.dto.*;
import GatewayManager.rest.GatewayController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GatewayControllerTest {

    @InjectMocks
    GatewayController controller;

    @Mock
    GatewayBS gatewayBS;

    @Test
    public void returnAllOccupiedAndAvailableDesksTest(){
        List<AllDesksAndOccupied> deskList = new ArrayList<AllDesksAndOccupied>();
        AllDesksAndOccupied desk = new AllDesksAndOccupied(1,1,1,1, "GA",1);
        deskList.add(desk);
        when(controller.returnAllOccupiedAndAvailableDesks("1")).thenReturn(deskList);
        assertEquals(deskList, gatewayBS.findOccupiedDesks("1"));
        verify(gatewayBS,times(1)).findOccupiedDesks("1");
    }

    @Test
    public void getDeskHistoryTest(){
        Date date = new Date(100000);
        OccupantHistory history = new OccupantHistory(1,"firstName","lastName","test01", date);
        List<OccupantHistory> historyList = new ArrayList<>();
        historyList.add(history);
        when(controller.getDeskHistory(1)).thenReturn(historyList);
        assertEquals(historyList,controller.getDeskHistory(1));
        verify(gatewayBS,times(1)).getDeskHistory(1);
    }

    @Test
    public void findPersonTest(){
        Date date = new Date(100000);
        OccupiedDesk deskOccupant = new OccupiedDesk(1,"firstName","lastName","test01","GA", date);
        List<OccupiedDesk> deskList = new ArrayList<>();
        deskList.add(deskOccupant);
        when(controller.findPerson("firstName", "lastName")).thenReturn(deskList);
        assertEquals(deskList, controller.findPerson("firstName","lastName"));
        verify(gatewayBS,times(1)).findPerson("firstName","lastName");
    }

    @Test
    public void getDesksByNeighbourhoodTest(){
        DeskByNeighbourhood desk = new DeskByNeighbourhood(1,1,"GA");
        controller.getDesksByNeighbourhood(1);
        verify(gatewayBS, times(1)).getDesksByNeighbourhood(1);
    }

}
