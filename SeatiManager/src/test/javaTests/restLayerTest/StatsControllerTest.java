package javaTests.restLayerTest;

import GatewayManager.business.StatsBS;
import GatewayManager.rest.StatsController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatsControllerTest {

    @InjectMocks
    StatsController controller;

    @Mock
    StatsBS statsBs;

    @Test
    public void getSectionsTest(){
        List<String> sections = new ArrayList<>();
        String section1 = "GA";
        String section2 = "GB";
        String section3 = "GC";

        sections.add(section1);
        sections.add(section2);
        sections.add(section3);

        when(statsBs.getSections()).thenReturn(sections);
        List<String>sectionList = controller.getSections();
        assertEquals(3,sectionList.size());
        verify(statsBs, times(1)).getSections();
    }

    @Test
    public void getCapacityTest(){
        controller.getCapacityCount();
        verify(statsBs,times(1)).getCapacityCount();
    }

    @Test
    public void getCapacityPerSectionTest(){
        controller.getCapacityPerSection();
        verify(statsBs, times(2)).getCapacityPerSection();
    }

    @Test
    public void suggestBetterTimeBasedOnCapacityTest(){
        controller.suggestBetterTimeBasedOnCapacity();
        verify(statsBs, times(1)).suggestBetterTimeBasedOnCapacity();
    }

    @Test
    public void averageCapacityByDateTest(){
        controller.averageCapacityByDate();
        verify(statsBs,times(1)).averageCapacityByDate();
    }

    @Test
    public void capacityCountOccupiedTest(){
        controller.getCapacityCountOccupied();
        verify(statsBs,times(1)).getCapacityCountOccupied();
    }

    @Test
    public void capacityCountDesksTest(){
        controller.getCapacityCountDesks();
        verify(statsBs, times(1)).getCapacityCountDesks();
    }

    @Test
    public void countOfEmployeesTest(){
        controller.getCountOfEmployees();
        verify(statsBs,times(1)).getCountOfEmployees();
    }

    @Test
    public void countOfAvailableDesksTest(){
        controller.getAvailableDeskCount();
        verify(statsBs, times(1)).getAvailableDeskCount();
    }




}
